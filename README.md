# vian

tool to run VIAN with on a set of files via G-CLI.

# to get list of changed files
``` bash
# get most recent tag
LAST_TAG=$(git describe --tags $(git rev-list --tags --max-count=1))
# get files changed since that tag - egrep filters for vi, vit, vim, and ctl files
CHANGED_FILES=$(git diff --name-only $LAST_TAG | egrep '*.(vi[tm]?|ctl)')
g-cli vian -- -config Path_to_vain_config $CHANGED_FILES
```

The real unresolved question is what should this tool do with failures? Fail? Fail if over a certain amount? hmmm.... I would like some kind of ratchet, but where and how do I store that number?

Maybe just judicious use of the QuickVIAN is the answer.
